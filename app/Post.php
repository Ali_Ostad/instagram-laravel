<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = [

        'user_id', 'caption'

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * Relation function between User Model and Post Model
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function likes()
    {
        return $this->belongsToMany(User::class , "likes");
    }
}
