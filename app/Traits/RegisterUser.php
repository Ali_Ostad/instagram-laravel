<?php


namespace App\Traits;


trait RegisterUser
{

    public function registerUser($fields)
    {
        $path = '/avatarPhotos/avatarIcone2.png';

        $user = \App\User::create([
            'name' => $fields->name,
            'email' => $fields->email,
            'type' => 0, // public
            'username' => $fields->username,
            'password' => $fields->password,
            'image' =>  $path,
            'search' => 'ali'
        ]);
    }

}
