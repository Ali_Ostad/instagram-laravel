<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

/**
 * Class User
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * @param $password
     *
     *  Hash the Password before storing to DB
     */

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'name', 'username', 'password', 'type' , 'image' , 'search'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     *  Relation function between User Model and Post Model
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getRouteKeyName()
    {
        return 'username';
    }

    public function likes()
    {
        return $this->belongsToMany(Post::class , "likes");
    }

    public function follower()
    {
        return $this->belongsToMany(User::class , "follows" , 'follower_id' , 'following_id');
    }

    public function following()
    {
        return $this->belongsToMany(User::class , "follows" , 'following_id' , 'follower_id')
            ->withTimestamps();
    }
}
