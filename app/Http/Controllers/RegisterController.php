<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Traits\RegisterUser;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    use RegisterUser;

    public function show()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $requestFields)
    {

        $user = $this->registerUser($requestFields);
        return redirect('/login');

    }
}
