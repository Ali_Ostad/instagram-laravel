<?php

namespace App\Http\Controllers;


use App\Like;
use App\Post_User;
use http\Env\Response;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function toggleLike(Request $request)
    {

        try{

            $user_id = $request->input('user_id');
            $post_id = $request->input('post_id');

            $like = Like::create([
                "user_id" => $user_id,
                "post_id" => $post_id
            ]);

            return response()->json([
                "data" => $like,
            ]);
        }catch (QueryException $e){
            $error = $e->errorInfo;
            return response()->json([
                "data" => $error,
            ]);
        }
    }

    public function toggleDislike(Request $request)
    {
        $user_id = $request->input('user_id');
        $post_id = $request->input('post_id');

        $dislike = Like::where([
            ['user_id', $user_id],
            ['post_id', $post_id]
        ])->delete();

        return response()->json([
            'data' => $dislike
        ]);
    }

}
