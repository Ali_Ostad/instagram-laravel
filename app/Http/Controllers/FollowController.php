<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FollowController extends Controller
{


    public function follow(User $username)
    {

        /**
         *
         */
        $follower_id = auth()->user()->id;
        $following_id = $username->id;

        $user = User::findOrFail($following_id);

        $user->following()->attach($follower_id);


        return redirect()->route('profile', ['username' => $username])->with('message', 'Followed Successfully');
    }

    public function unFollow(User $username)
    {
        $follower_id = auth()->user()->id;
        $following_id = $username->id;

        $user = User::findOrFail($following_id);
        $user->following()->detach($follower_id);


        return redirect()->route('profile', ['username' => $username]);

    }


    public function followers(User $username)
    {
        $followers = $username->following()->get();

        return view('follow.follower', ['followers' => $followers]);
    }

    public function followings(User $username)
    {
        $followings = $username->follower()->get();

        return view('follow.following', ['followings' => $followings]);
    }


    /*    public function showRequest()
        {
            return view('follow.followRequest');
        }*/

}
