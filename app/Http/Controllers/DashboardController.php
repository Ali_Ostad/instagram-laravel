<?php

namespace App\Http\Controllers;

use App\Follow;
use App\Http\Requests\ProfileEditRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use function foo\func;

class DashboardController extends Controller
{
    public function index(User $username)
    {

        /**
         *  Send Post Info For Show In Profile Page
         */
        $posts = $username->where('username', $username->username)->first()->posts()->with('photos')->get();
        $postsCount = count($posts);

        /**
         *  Check for Show Follow Or UnFollow Form
         */
        $user = Follow::where([
            'following_id' => $username->id,
            'follower_id' => auth()->id()
        ])->first();

        /**
         * Count of followings
         */
        $following = Follow::where('follower_id', $username->id)->get();
        $followingCount = count($following);


        /**
         *  Count of Followers
         */

        $follower = Follow::where('following_id', $username->id)->get();
        $followerCount = count($follower);


        return view('profile.profile', ['posts' => $posts
            , 'username' => $username
            , 'user' => $user
            , 'followingCount' => $followingCount
            , 'followerCount' => $followerCount
            , 'postsCount' => $postsCount]);

    }

    public function editProfile()
    {
        $userInfo = User::where('id', auth()->user()->id)->first();


        return view('profile.profile-edit', compact('userInfo'));
    }

    public function edit(ProfileEditRequest $editRequest)
    {

        /**
         * avatarImage upload and Update
         */

        if (!is_null(request('avatarImage'))) {
            $filename = 'Avatar-Image' . '-' . time() . '.' . $editRequest['avatarImage']->getClientOriginalExtension();
            $path = $editRequest['avatarImage']->storeAs('/avatarPhotos', $filename, 'local_storage');


            /**
             *  Update User Information in DB
             */

            $user_update = User::where('id', auth()->user()->id)->update([
                'image' => $path,
                'name' => $editRequest['name'],
                'username' => $editRequest['username'],
                'email' => $editRequest['email'],
                'website' => $editRequest['website'],
                'bio' => $editRequest['bio'],
                'phone_number' => $editRequest['phoneNumber'],
                'gender' => $editRequest['gender'],
            ]);

            return redirect('/accounts/edit')->with('status', 'Profile Updated');
        } else {
            $user_update = User::where('id', auth()->user()->id)->update([
                'name' => $editRequest['name'],
                'username' => $editRequest['username'],
                'email' => $editRequest['email'],
                'website' => $editRequest['website'],
                'bio' => $editRequest['bio'],
                'phone_number' => $editRequest['phoneNumber'],
                'gender' => $editRequest['gender'],
            ]);

            return redirect('/accounts/edit')->with('status', 'Profile Updated');

        }
    }
}
