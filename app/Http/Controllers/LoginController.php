<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function show()
    {
        return view('auth.login');
    }

    public function authenticate(LoginRequest $requestFields)
    {
        $attributes = $requestFields->only(['username' , 'password']);


        if (Auth::attempt($attributes)) {
            return redirect()->route('home');
        }
    }

    public function logout()
    {
        session()->flush();
        Auth::logout();
        return back();
    }

}
