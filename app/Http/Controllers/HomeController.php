<?php

namespace App\Http\Controllers;

use App\Follow;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\EditPostRequest;
use App\Photo;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function home()
    {
        /**
         *  Pass Post's Data to View
         */
        $posts = auth()->user()->posts()->with('photos')->with('likes')->orderBy('id', 'desc')->get();


        $user = User::where('id', auth()->id())->first();


        return view('home', ['posts' => $posts, 'user' => $user]);
    }

    public function show(User $username , $id)
    {
        $users = $username->where('username' , $username->username)->with('posts')->get();

        $posts = $username->posts()->where('id' , $id)->with('photos')->get();

        return view('profile.showPost' , ['users' => $users , 'posts' => $posts]);
    }


    public function create()
    {
        return view('create-post');
    }

    public function store(CreatePostRequest $validatePost)
    {
        /**
         * First : validate inputs Post (image and caption)
         */


        /**
         *  Second : store image file in storage(photos) with the name we want
         */


        $filename = 'Post-Photo' . '-' . time() . '.' . $validatePost['file']->getClientOriginalExtension();
        $path = $validatePost['file']->storeAs('/photos', $filename, 'local_storage');

        /**
         *  Create Post in DB
         */

        $post = Post::create([
            'user_id' => auth()->id(),
            'caption' => $validatePost['caption']
        ]);


        /**
         *  Select post_id and create Photo
         */


        Photo::create([
            'post_id' => $post->id,
            'path' => $path
        ]);


        return redirect('/home');


    }

    public function edit($id)
    {
        $post = Post::find($id);

        return view('profile.editPost' , ['post'=>$post]);
    }

    public function update(EditPostRequest $request , $id)
    {
        $post = Post::find($id);

        $post->update([
            'caption' => $request->caption
        ]);

        $username = auth()->user()->username;

        return redirect()->route('profile' , ['username' => $username]);
    }

    public function destroy($id)
    {
        $post = Post::with('photos')->find($id);
        $post->delete();

        return redirect('/home');
    }
}
