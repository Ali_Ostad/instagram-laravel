<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'avatarImage' => 'nullable|file|image|mimes:jpeg,png,gif,webp|max:2048',
            'name' => 'required|string|min:1|max:255',
            'username' => 'required|string',
            'email' => ['required','string','email','max:255',Rule::unique('users')->ignore(auth()->user()->id),],
            'website' => 'nullable|url|max:255',
            'bio' => 'nullable|string|max:255',
            'phoneNumber' => 'nullable|numeric',
            'gender' => 'nullable',

        ];
    }
}
