<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'post_id', 'path'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class , 'post_id');
    }
}
