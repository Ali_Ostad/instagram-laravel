<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Illuminate\Support\Facades\Auth;

//Auth::routes();


/*Route::prefix('home')->group(function () {
    Route::get('/', "HomeController@home");
});*/

/**
 * Route for Welcome Page
 */

Route::get('/', "HomeController@welcome")->name('welcome');


/**
 *  Routes for login and registration
 */

/**
 *  Show register page and login page
 */
Route::get('/login', "LoginController@show")
    ->name('login')
    ->middleware('guest');

Route::get('/register', "RegisterController@show")
    ->name('register')
    ->middleware('guest');
/**
 *   Register and Login User
 */

Route::post('/login', "LoginController@authenticate");
Route::post('/register', "RegisterController@register")->name('register');


/**
 *  Protected Routes - allows only logged in users
 */

Route::middleware('auth')->group(function () {
    Route::get('/home', "HomeController@home")->name('home');

    Route::post('/logout', "LoginController@logout")->name('logout');
});


/**
 *  Route for Seven Restful CRUD Post (resource)
 */

Route::get('/post/create', "HomeController@create")->name('create')->middleware('auth');
Route::post('/create-post', "HomeController@store")->name('store')->middleware('auth');

Route::get('/{username}/post/{id}' , 'HomeController@show')->name('show');

Route::get('post/{id}/edit' , 'HomeController@edit')->name('edit')->middleware('auth');
Route::put('post/{id}' , 'HomeController@update')->name('update');

Route::delete('post/{id}' , 'HomeController@destroy');

/**
 *  Route for Show Profile Page
 */

Route::get('/profile/{username}', "DashboardController@index")->name('profile');

/**
 *  Route for Show and Edit Profile Page
 */

Route::get('/accounts/edit', "DashboardController@editProfile")->name('profileEdit')->middleware('auth');

Route::put('/accounts/edit', "DashboardController@edit")->name('edit');



/**
 *  Route For Follow and UnFollow Requests
 */

Route::post('/follow/{username}' , "FollowController@follow")->name('follow');
Route::delete('/unFollow/{username}' , "FollowController@unFollow")->name('unFollow');


/**
 *  Route For Show List Of follower and Following
 */

Route::get('/{username}/followers' , 'FollowController@followers');
Route::get('/{username}/followings' , 'FollowController@followings');


//Route::get('/follow/request' , "FollowController@showRequest")->name('followRequest');

