<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('search');
            $table->string('password');
            $table->string('username');
            $table->string('name', 45);
            $table->string('phone_number', 45)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('bio')->nullable();
            $table->string('website')->nullable();
            $table->string('image', 255)->nullable();
            $table->string('gender' , 20)->nullable();
            $table->integer('type')->default(0)->unsigned();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
