@extends('layouts.master-home.master-home')

@section('title' , "Profile")


@section('content')

    <main class="profile-container">
        <section class="profile">
            <header class="profile__header">
                <div class="profile__avatar-container">
                    <a href="{{ route('profileEdit') }}">
                        <img
                            src="{{ asset('storage/' . $username->image)  }}"
                            class="profile__avatar"
                        />
                    </a>
                </div>
                <div class="profile__info">
                    <div class="profile__name">
                        <h1 class="profile__title">{{ $username->username }}</h1>
                        @if(auth()->check())
                            @if(auth()->user()->username == $username->username)
                                <a href="{{ route('profileEdit') }}" class="profile__button u-fat-text">Edit profile</a>

                            @elseif($user)
                                <form action="{{ route('unFollow' , ['username' => $username->username]) }}"
                                      method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="profile__button__unFollower u-fat-text">UnFollow</button>
                                </form>
                            @else
                                <form action="{{ route('follow' , ['username' => $username->username]) }}"
                                      method="post">
                                    @csrf
                                    <button class="profile__button__follower u-fat-text">Follow</button>
                                </form>
                            @endif
                        @elseif($user)
                            <form action="{{ route('unFollow' , ['username' => $username->username]) }}"
                                  method="post">
                                @csrf
                                @method('delete')
                                <button class="profile__button__unFollower u-fat-text">UnFollow</button>
                            </form>
                        @else
                            <form action="{{ route('follow' , ['username' => $username->username]) }}" method="post">
                                @csrf
                                <button class="profile__button__follower u-fat-text">Follow</button>
                            </form>
                        @endif
                        <i class="fa fa-cog fa-2x" id="cog"></i>
                    </div>
                    <ul class="profile__numbers">
                        <li class="profile__posts">
                            <span class="profile__number u-fat-text">{{ $postsCount }}</span> posts
                        </li>
                        <li class="profile__followers">
                            <span class="profile__number u-fat-text">{{$followerCount}}</span> <a href="/{{$username->username}}/followers">followers</a>
                        </li>
                        <li class="profile__following">
                            <span class="profile__number u-fat-text">{{$followingCount}}</span> <a href="/{{$username->username}}/followings">following</a>
                        </li>
                    </ul>
                    <div class="profile__bio">
                        <div>
                            <span class="profile__full-name u-fat-text">{{ $username->name }}</span>
                        </div>
                        <div>
                            <p class="profile__full-bio">{{ $username->bio }}</p>
                        </div>
                        <div>
                            <a href="http://serranoarevalo.com"
                               class="profile__link u-fat-text">{{ $username->website }}</a>
                        </div>
                    </div>
                </div>
            </header>
            <div class="profile__pictures">
                @foreach($posts as $post)

                    <a href="{{ route('show' , ['username'=>$username->username , 'id'=>$post->id]) }}"
                       class="profile-picture">
                        <div class="column">
                            @foreach($post->photos as $photo)

                                <img
                                    src="{{ asset('storage/'. $photo->path) }}"
                                    class="profile-picture__picture"
                                />
                            @endforeach
                        </div>
                        <div class="profile-picture__overlay">
                            <span class="profile-picture__number">
                                <i class="fa fa-heart"></i> 450
                            </span>
                            <span class="profile-picture__number">
                                <i class="fa fa-comment"></i> 39
                            </span>
                        </div>
                    </a>

                @endforeach
            </div>

        </section>
    </main>
@endsection
