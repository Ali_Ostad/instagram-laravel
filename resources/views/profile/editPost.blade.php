@extends('layouts.master-home.master-home')


@section('title' , 'Edit Post')



@section('content')


    <main class="edit-profile">
        <section class="profile-form">
            <div>
                <h1 class="edit-page-heading">Edit Your Post</h1>
            </div>
            <form action="{{ route('update' , ['id'=>$post->id]) }}" class="edit-profile__form" method="POST"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="edit-profile__form-row">
                    <label for="bio" class="edit-profile__label">
                        New Caption :
                    </label>
                    <textarea id="caption" name="caption"
                              class="@error('caption') is-invalid @enderror
                                  edit-profile__textarea">
                        {{old('caption' , $post->caption)}}
                    </textarea>
                    @error('caption')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>


                <div class="edit-profile__form-row">
                    <label class="edit-profile__label"></label>
                    <button type="submit" name="upload" class="fsSubmitButton">Upload</button>
                </div>
            </form>
        </section>
    </main>



@endsection
