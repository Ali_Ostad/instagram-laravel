@extends('layouts.master-home.master-home')


@section('title' , "Profile-Edit")


@section('content')


    <main class="edit-profile">
        <section class="profile-form">
            <form action="{{ route('edit') }}" enctype="multipart/form-data" class="edit-profile__form"
                  method="post">
                <div>
                    @if(session('status'))
                        <div id="alert">
                            <span>{{ session('status') }}</span>
                        </div>
                    @endif
                </div>
                <br>
                <br>
                @method('PUT')
                @csrf
                <header class="profile-form__header">

                    <div class="image-upload profile-form__avatar-container">
                        <label for="file-input">
                            <img src="/images/avatarIcone2.png" class="profile-form__avatar"/>
                        </label>
                        <input class="@error('avatarImage') is-invalid @enderror"
                               name="avatarImage"
                               value="{{ old('avatarImage' , $userInfo->image) }}"
                               id="file-input"
                               type="file"/>

                        @error('avatarImage')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <h4 class="profile-form__title">Upload Your Image</h4>

{{--                    {{ dump($errors) }}--}}
                </header>


                <br>
                <div class="edit-profile__form-row">
                    <label for="name" class="edit-profile__label">Name</label>
                    <input
                        id="name"
                        name="name"
                        type="text"
                        value="{{ old('name' , $userInfo->name) }}"
                        class="@error('name') is-invalid @enderror edit-profile__input"/>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="edit-profile__form-row">
                    <label for="username" class="edit-profile__label">Username</label>
                    <input
                        name="username"
                        type="text"
                        value="{{ old('username' , $userInfo->username) }}"
                        id="username"
                        class="@error('username') is-invalid @enderror edit-profile__input"/>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="edit-profile__form-row">
                    <label for="username" class="edit-profile__label">Email</label>
                    <input
                        name="email"
                        type="email"
                        value="{{ old('email' , $userInfo->email) }}"
                        id="username"
                        class="@error('email') is-invalid @enderror edit-profile__input"/>
                    @error('username')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="edit-profile__form-row">
                    <label for="website" class="edit-profile__label">
                        Website
                    </label>
                    <input
                        name="website"
                        type="url"
                        value="{{ old('website' , $userInfo->website) }}"
                        id="website"
                        class="@error('website') is-invalid @enderror edit-profile__input"/>
                    @error('website')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="edit-profile__form-row">
                    <label for="bio" class="edit-profile__label">
                        Bio
                    </label>
                    <textarea
                        name="bio"
                        id="bio"
                        class="@error('bio') is-invalid @enderror edit-profile__textarea">
                            {{ old('bio' , $userInfo->bio) }}</textarea>
                    @error('bio')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="edit-profile__form-row">
                    <label for="phone-number" class="edit-profile__label">
                        Phone Number
                    </label>
                    <input
                        name="phoneNumber"
                        value="{{ old('phoneNumber' , $userInfo->phone_number) }}"
                        type="text"
                        class="@error('phoneNumber') is-invalid @enderror edit-profile__input"
                        id="phone-number"/>
                    @error('phoneNumber')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="edit-profile__form-row">
                    <label for="gender" class="edit-profile__label">Gender</label>
                    <select class="@error('gender') is-invalid @enderror" name="gender" id="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option selected value="whatever">Whatever</option>
                    </select>
                    @error('gender')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="edit-profile__form-row">
                    <span class="edit-profile__label">Similar Account Suggestions</span>
                    <input type="checkbox" id="similar" checked>
                    <label for="similar">Include your account when recommending similar accounts people might want
                        to
                        follow. <a href="#">[?]</a></label>
                </div>
                <div class="edit-profile__form-row">
                    <label class="edit-profile__label"></label>
                    <input name="update" type="submit" value="Update">
                </div>
            </form>
        </section>
    </main>


@endsection
