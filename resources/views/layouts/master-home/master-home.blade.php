<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title' , "Home") | Instagram</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link href="/css/style.css" rel="stylesheet">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <style>

        .input-file-container {
            position: relative;
            width: 225px;
        }

        .js .input-file-trigger {
            display: block;
            padding: 14px 45px;
            background: #3897f0;
            color: #ffff;
            font-size: 1em;
            transition: all .4s;
            cursor: pointer;
        }

        .js .input-file {
            position: absolute;
            top: 0;
            left: 0;
            width: 225px;
            opacity: 0;
            padding: 14px 0;
            cursor: pointer;
        }

        .js .input-file:hover + .input-file-trigger,
        .js .input-file:focus + .input-file-trigger,
        .js .input-file-trigger:hover,
        .js .input-file-trigger:focus {
            background: #34495E;
            color: #3897f0;
        }

        .file-return {
            margin: 0;
        }

        .file-return:not(:empty) {
            margin: 1em 0;
        }

        .js .file-return {
            font-style: italic;
            font-size: .9em;
            font-weight: bold;
        }

        .js .file-return:not(:empty):before {
            content: "Selected file: ";
            font-style: normal;
            font-weight: normal;
        }

        .fsSubmitButton {
            padding: 10px 15px 11px !important;
            font-size: 18px !important;
            background-color: #34495E;
            font-weight: bold;
            text-shadow: 1px 1px #34495E;
            color: #ffff;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border: 1px solid #34495E;
            cursor: pointer;
            box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
            -moz-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
            -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
        }

    </style>
</head>
<body>
<nav class="navigation">
    <a href="{{ route('home') }}">
        <img src="/images/navLogo.png" alt="logo" title="logo" class="navigation__logo"/>
    </a>
    <div class="navigation__search-container">
        <i class="fa fa-search"></i>
        <input type="text" placeholder="Search">
    </div>
    <div class="navigation__icons">
        <a href="{{ route('create') }}" class="navigation__link">
            <i class="fa fa-plus-square"></i>
        </a>
        <a href="#" class="navigation__link">
            <i class="fa fa-compass"></i>
        </a>
        <a href="#" class="navigation__link">
            <i class="fa fa-heart-o"></i>
        </a>
        @if(auth()->check())
            <a href="{{ route('profile', ['username' => auth()->user()->username ])}}" class="navigation__link">
                <i class="fa fa-user-o"></i>
            </a>
        @endif
    </div>
</nav>

@yield('content')

<footer class="footer">
    <nav class="footer__nav">
        <ul class="footer__list">
            <li class="footer__list-item"><a href="#" class="footer__link">about us</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">support</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">blog</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">press</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">api</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">jobs</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">privacy</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">terms</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">directory</a></li>
            <li class="footer__list-item"><a href="#" class="footer__link">language</a></li>
        </ul>
    </nav>
    <span class="footer__copyright">© 2020 instagram</span>
</footer>
<script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
<script src="/js/app.js"></script>
<script src="/js/insta.js"></script>
<script>
    document.querySelector("html").classList.add('js');

    var fileInput = document.querySelector(".input-file"),
        button = document.querySelector(".input-file-trigger"),
        the_return = document.querySelector(".file-return");

    button.addEventListener("keydown", function (event) {
        if (event.keyCode == 13 || event.keyCode == 32) {
            fileInput.focus();
        }
    });
    button.addEventListener("click", function (event) {
        fileInput.focus();
        return false;
    });
    fileInput.addEventListener("change", function (event) {
        the_return.innerHTML = this.value;
    });
</script>
</body>
</html>
