<html>
<head>
    <meta charset="UTF-8">
    <title>Login | Instagram</title>
    <link rel="shortcut icon" type="image/x-icon" href="instagram-logo.ico.png" sizes="16x16">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine">

    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui.js"></script>

    <style>

        /* Reset CSS */
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, font, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: baseline;
            background: transparent;
        }

        body {
            background: #FAFAFA;
            font-family: 'Open Sans', sans-serif;
        }

        #content {
            background: white;
            border: thin solid #F5F5F5;
            margin: 0 auto;
            padding: 10px 0px 25px;
            position: relative;
            text-align: center;
            width: 348px;
            top: 10%;
        }

        #no_account {
            background: white;
            border: thin solid #F5F5F5;
            margin: 0 auto;
            padding: 25px 50px 25px 50px;
            position: relative;
            text-align: center;
            width: 248px;
            bottom: 0%;
            top: 11%;
        }

        #no_account a {
            color: #62B1F6;
            position: relative;
            letter-spacing: -.5px;
        }

        #get_app {
            position: relative;
            margin: 0 auto;
            text-align: center;
            width: 100px;
            top: 15%;
        }

        #photos {
            position: relative;
            margin: 0 auto;
            text-align: center;
            width: 33%;
            bottom: 0%;
            top: 20%;
            left: 33%;
            float: left;
            display: inline-block;
        }

        h2 {
            color: black;
            font-size: 70px;
            letter-spacing: -.02em;
            line-height: 20px;
            margin: 20px 10px 32px;
            font-family: "Tangerine";
        }

        h3, h4 {
            font-size: 15px;
            font-weight: lighter;
        }

        a {
            text-decoration: none;
        }

        #password_username {
            width: 100%;
        }

        #content form input[type="password"] {
            width: 85%;
            left: 2px;
            outline-width: .05px;
            outline-color: #d3d3d3;

        }

        #content form input[type="text"] {
            width: 85%;
            outline-color: #d3d3d3;
            outline-width: .05px;

        }

        #content form input[type="text"],
        #content form input[type="password"],
        #content form input[type="submit"] {
            border-radius: 3px;
            border: thin solid #d3d3d3;
            font-weight: lighter;
            margin: 0px 0 7px 5px;
            padding: 10px 5px 5px 5px;
            font-size: 13px;
            position: relative;
        }

        #lnkforget {
            position: relative;
            /*left: 261px;
            bottom: 75px;
                            width: 5%;
*/
            color: #3B3B3B;
            font-size: 14px;
            left: 115px;
            bottom: 30px;
            width: 5%;
        }

        #content form input[type="submit"] {
            background: #5CACEE;
            color: white;
            font-size: 15px;
            border: none;
            text-align: center;
            position: relative;
            width: 85%;
            font-weight: normal;
            padding: 6px;
        }

        /* BEGINNING OF JQUERY/JS CSS*/
        #error1, #error2 {
            color: red;
            font-size: 15px;
            position: relative;
            width: 85%;
            padding: 15px 20px 30px 15px;
            text-align: center;
            left: 3%;
        }

        .footer {
            width: 90%;
            max-width: 1000px;
            margin: 0 auto;
            margin-top: 90px;
            margin-bottom: 30px;
            display: flex;
            align-items: center;
            justify-content: space-between;
            flex-wrap: wrap;
            text-transform: uppercase;
            font-size: 12px;
            font-weight: 700;
        }

        .footer__list {
            display: flex;
            flex-wrap: wrap;
        }

        .footer__list-item {
            margin-right: 15px;
        }

        .footer__link {
            color: #003569;
        }

        .footer__copyright {
            color: #999;
        }

        .footer-mine {

        }
    </style>
</head>
<body>
<section id="content">
    <div id="password_username">
        <form action="{{ route('login') }}" method="POST">
            @csrf
            <h2>Instagram</h2>
            <div>
                <input type="text" placeholder="Username" name="username" id="username"
                       class="@error('username') is-invalid @enderror" value="{{ old('username') }}"
                       autocomplete="username" autofocus/>

                @error('username')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div>
                <input type="password" placeholder="Password" id="password" name="password"
                       class="@error('password') is-invalid @enderror" value="{{ old('password') }}"
                       autocomplete="password"
                       autofocus/>

                @error('password')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div>
                <a href="https://www.instagram.com/accounts/password/reset/" id="lnkforget">Forgot?</a>
            </div>
            <input type="submit" value="Log in" id="submit"/>
        </form>
    </div>
</section>
<div id="no_account">
    <h4>Don't have an account?<a href="{{ route('register') }}"> Sign up</a></h4>
</div>
<div id="get_app">
    <h3>Get the app.</h3>
</div>
<div id="photos">
    <a href="https://itunes.apple.com/app/instagram/id389801252?mt=8" target="_blank"><img
            src="http://gtpie.com/wp-content/uploads/2015/07/appStoreIcon.png" width="127" height="40"></a>
    <a href="https://play.google.com/store/apps/details?id=com.instagram.android&referrer=utm_source%3Dinstagramweb%26utm_campaign%3DloginPage%26utm_medium%3Dbadge"
       taret="_blank"><img src="http://www.codecu.org/Images/2016_05_googlePlay.png" width="127" height="40"></a>
    <div class="footer-mine">
        <footer class="footer">
            <nav class="footer__nav">
                <ul class="footer__list">
                    <li class="footer__list-item"><a href="#" class="footer__link">about us</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">support</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">blog</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">press</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">api</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">jobs</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">privacy</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">terms</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">directory</a></li>
                    <li class="footer__list-item"><a href="#" class="footer__link">language</a></li>
                </ul>
            </nav>
            <span class="footer__copyright">© 2017 instagram</span>
        </footer>
    </div>
</div>
</body>
</html>

