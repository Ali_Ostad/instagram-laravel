@extends('layouts.master-home.master-home')


@section('title' , 'Follower List')




@section('content')

    <main class="explore">
        <section class="people">
            <ul class="people__list">
                @foreach($followers as $follower)
                    <li class="people__person">
                        <div class="people__column">
                            <div class="people__avatar-container">
                                <img
                                    src="{{ asset('/storage/'.$follower->image) }}"
                                    class="people__avatar"
                                />
                            </div>
                            <div class="people__info">
                                <span class="people__username">{{ $follower->username }}</span>
                                <span class="people__full-name">{{ $follower->name }}</span>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </section>
    </main>


@endsection
