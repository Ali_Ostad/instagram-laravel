@extends('layouts.master-home.master-home')


@section('title' , 'Follower List')




@section('content')

    <main class="explore">
        <section class="people">
            <ul class="people__list">
                @foreach($followings as $following)
                <li class="people__person">
                    <div class="people__column">
                        <div class="people__avatar-container">
                            <img
                                src="{{asset('/storage/'.$following->image)}}"
                                class="people__avatar"
                            />
                        </div>
                        <div class="people__info">
                            <span class="people__username">{{ $following->username }}</span>
                            <span class="people__full-name">{{ $following->name }}</span>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </section>
    </main>


@endsection
