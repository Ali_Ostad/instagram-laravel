@extends('layouts.master-home.master-home')

@section('title' , "Follow Request")



@section('content')

    <main class="explore">
        <section class="people">
            <ul class="people__list">
               @foreach($following as $value)

                    <li class="people__person">
                        <div class="people__column">
                            <div class="people__avatar-container">
                                <img
                                    src="{{ asset('storage/' . $value->image) }}"
                                    class="people__avatar"
                                />
                            </div>
                            <div class="people__info">
                                <span class="people__username">{{ $value->username }}</span>
                                <span class="people__full-name">{{ $value->name }}</span>
                            </div>
                        </div>
                        <div class="people__column">
                            <a href="#">Follow</a>
                        </div>
                    </li>
               @endforeach
            </ul>
        </section>
    </main>

@endsection
