@extends('layouts.master-home.master-home')


@section('title' , "Create-Post")


@section('content')


    <main class="edit-profile">
        <section class="profile-form">
            <form action="{{ route('store') }}" class="edit-profile__form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="edit-profile__form-row">
                    <label for="" class="edit-profile__label">Upload Image :</label>
                    <div class="input-file-container">
                        <input class="@error('file') is-invalid @enderror input-file" name="file" id="my-file" type="file">
                        @error('file')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <label tabindex="0" for="my-file" class="input-file-trigger">Select a file...</label>
                    </div>
                </div>

                <div class="edit-profile__form-row">
                    <label for="bio" class="edit-profile__label">
                        Caption :
                    </label>
                    <textarea id="caption" name="caption" class="@error('caption') is-invalid @enderror edit-profile__textarea"></textarea>
                    @error('caption')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>


                <div class="edit-profile__form-row">
                    <label class="edit-profile__label"></label>
                    <button type="submit" name="upload" class="fsSubmitButton">Upload</button>
                </div>
            </form>
        </section>
    </main>

@endsection
