@extends('layouts.master-home.master-home')

@section('title' , "Home")
@section('content')
    <main class="feed">

        @foreach($posts as $post)

            <section class="photo">
                <header class="photo__header">
                    <div class="photo__header-column">
                        <img
                            class="photo__avatar"
                            src="{{ asset('storage/' . $user->image) }}"
                        />
                    </div>
                    <div class="photo__header-column">
                        <span class="photo__username">{{ $user->username }}</span>
                        {{--                        <span class="photo__location">European Art of Living Center - Bad Antogast</span>--}}
                    </div>
                </header>
                <div class="photo__file-container">
                    @foreach($post->photos as $photo)
                        <img
                            class="photo__file"
                            src="{{ asset('storage/'. $photo->path) }}"
                        />
                    @endforeach
                </div>
                <div class="photo__info">
                    <div class="photo__icons">
                        <span class="photo__icon">
                            @if(count($post->likes) > 0)
                                @foreach($post->likes as $like)
                                    @if($like->pivot->user_id == auth()->user()->id)
                                        <i class="fa heart fa-lg heart-red fa-heart"
                                           onclick="handleDislike(this , '{{ $post->id }}' , '{{ auth()->user()->id }}')"></i>
                                    @else
                                        <i class="fa fa-heart-o heart fa-lg"
                                           onclick="handleLike(this ,'{{ $post->id }}' , '{{ auth()->user()->id }}')"></i>
                                    @endif
                                @endforeach
                            @else
                                <i class="fa fa-heart-o heart fa-lg"
                                   onclick="handleLike(this ,'{{ $post->id }}' , '{{ auth()->user()->id }}')"></i>
                            @endif
                        </span>
                        <span class="photo__icon">
                            <i class="fa fa-comment-o fa-lg"></i>
                        </span>
                    </div>
                    <span class="photo__likes" id="like_count">{{ count($post->likes) }}likes</span>
                    <ul class="photo__comments">
                        <li class="photo__comment">
                            <span class="photo__comment-author">{{ $user->username }}</span>{{ $post['caption'] }}
                        </li>
                        <li class="photo__comment">
                            <span class="photo__comment-author">Comments</span>
                        </li>
                    </ul>
                    <span class="photo__time-ago">11 hours ago</span>
                    <div class="photo__add-comment-container">
                        <textarea placeholder="Add a comment..." class="photo__add-comment"></textarea>
                        <i class="fa fa-ellipsis-h"></i>
                    </div>
                </div>
            </section>
        @endforeach


    </main>
    @if(auth()->check())
        <form action="{{ route('logout') }}" method="post">
            @csrf
            <button class="btn-danger">logout</button>
        </form>
    @else
        <button class="btn-danger"><a href="{{ route('login') }}" class="btn btn-info">login</a></button>
    @endif

@endsection
