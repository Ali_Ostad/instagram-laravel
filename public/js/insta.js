$(document).ready(function () {
    // Here we put all the code
    var cog = $('#cog'),
        popUp = $('.popUp'),
        closePopUp = $('#closePopUp'),
        cancelPopUp = $('#cancelPopUp');


    cog.click(function () {
        popUp.fadeIn(500);
    });

    closePopUp.click(function () {
        popUp.fadeOut(500);
    });

    cancelPopUp.click(function () {
        popUp.slideUp(500)
    })

});


function handleLike(obj, post_id, user_id) {
    $(obj).toggleClass('fa-heart-o');
    $(obj).toggleClass('heart-red fa-heart');


    $.ajax({
        type: 'POST',
        url: 'api/toggleLike',
        data: {
            post_id,
            user_id
        },
        success: function (response) {
            console.log("response", response);
            let count = document.getElementById('like_count').innerText;
            let newCount = parseInt(count);

            newCount += 1;
            $('#like_count').text(`${newCount} likes`);
        },
        error: function (error) {
            console.log("error", error);
        }

    })
}

function handleDislike(obj, post_id, user_id) {
    console.log(post_id, user_id)
}


function toggleLike() {


}

function toggleDislike() {
    $.ajax({
        type: 'DELETE',
        url: 'api/toggleDislike',
        data: {
            post_id: 1,
            user_id: 1
        },
        success: function (response) {
            console.log("response", response);
        },
        error: function (error) {
            console.log("error", error);
        }

    })
}


function handleFollow(followerId, followingId) {
    $.ajax({
        type: 'POST',
        url: '/api/user/follow',
        data: {
            follower_id: followerId,
            following_id: followingId
        },
        success: function (response) {
            console.log("response", response);
        },
        error: function (error) {
            console.log("error", error);
        }

    })
}
